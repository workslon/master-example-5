var React = require('react');

module.exports = React.createClass({
  getInitialState: function () {
    var data = this.props.data || [];
    var selectedItems = this.props.selectedItems || [];
    var dropdownItems = data.filter(function(item) {
      return selectedItems.filter(function(selectedItem) {
        return selectedItem.objectId !== item.objectId;
      }).length
    });

    !selectedItems.length && (dropdownItems = data);

    return {
      dropdownItems: dropdownItems,
      selectedItems: selectedItems
    }
  },

  onClick: function (e) {
    // remove the item from the "selected items" list and
    // add it to the dropdown list
    var dropdownItems = this.state.dropdownItems;
    var selectedItems = this.state.selectedItems;
    var val = e.target.parentNode.getAttribute('data-value');

    dropdownItems.push(
      selectedItems.filter(function (item) {
        return item.objectId === val
      })[0]
    );

    selectedItems = selectedItems.filter(function (item) {
      return item.objectId !== val;
    });

    this.setState({
      selectedItems: selectedItems,
      dropdownItems: dropdownItems
    }, function () {
      this.props.onChange && this.props.onChange(this.state);
    });
  },

  onChange: function (e) {
    // remove the item from the dropdown list and
    // add it to the "selected items" list
    if (!e.target.value) return;
    var dropdownItems = this.state.dropdownItems;
    var selectedItems = this.state.selectedItems;
    var val = e.target.value;

    selectedItems.push(
      dropdownItems.filter(function (item) {
        return item.objectId === val
      })[0]
    );

    dropdownItems = dropdownItems.filter(function (item) {
      return item.objectId !== val;
    });

    this.setState({
      selectedItems: selectedItems,
      dropdownItems: dropdownItems
    }, function () {
      this.props.onChange && this.props.onChange(this.state);
    });
  },
  render: function () {
    var onClick = this.onClick;
    var selectedItems = this.state.selectedItems;
    var dropdownItems = this.state.dropdownItems;

    return (
      <div className="form-group" id="multi-select">
        {/* "selected items" list */}
        <ul ref="selectedItems">{selectedItems.map(function (item) {
          return (
            <li
              data-value={item.objectId}
              key={item.objectId}>{item.name}
              <span className="delete-selected-item"
                onClick={onClick}>×</span>
            </li>)
        })}</ul>

        {/* single-selection list */}
        <select className="form-control" ref="dropdownItems" onChange={this.onChange}>
          <option value="">-select author-</option>
           {dropdownItems.map(function (item) {
             return (
              <option
                key={item.objectId}
                value={item.objectId}>{item.name}</option>)
           })}
        </select>
      </div>
    );
  }
});