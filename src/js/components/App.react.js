var React = require('react');
var ReactRouter = require('react-router');
var BookStore = require('../stores/BookStore');
var BookActions = require('../actions/BookActions');
var BookModel = require('../models/Book');
var PublisherStore = require('../stores/PublisherStore');
var PublisherActions = require('../actions/PublisherActions');
var PublisherModel = require('../models/Publisher');
var AuthorStore = require('../stores/AuthorStore');
var AuthorActions = require('../actions/AuthorActions');
var AuthorModel = require('../models/Author');
var StatusConstants = require('../constants/StatusConstants');
var NotificationStore = require('../stores/NotificationStore');
var NotificationActions = require('../actions/NotificationActions');
var IndexLink = ReactRouter.IndexLink;

module.exports = React.createClass({
  displayName: 'App',

  childContextTypes: {
    router: React.PropTypes.object.isRequired
  },

  contextTypes: {
    router: React.PropTypes.object.isRequired
  },

  getInitialState: function () {
    return {
      books: BookStore.getAllBooks(),
      publishers: PublisherStore.getAllPublishers(),
      authors: AuthorStore.getAllAuthors(),
      notifications: NotificationStore.getNotifications()
    }
  },

  componentDidMount: function () {
    BookActions.getBooks(BookModel);
    PublisherActions.getPublishers(PublisherModel);
    AuthorActions.getAuthors(AuthorModel);

    this.context.router.listenBefore(this._clearNotifications);

    BookStore.addChangeListener(this._onChange);
    PublisherStore.addChangeListener(this._onChange);
    AuthorStore.addChangeListener(this._onChange);
    NotificationStore.addChangeListener(this._onChange);

    // activate the right tab
    var pathname = this.props.location.pathname.split('/')[1];

    if (pathname) {
      this._activate(this.refs[pathname]);
    } else {
      this._activate(this.refs.books);
    }
  },

  componentWillUnmount: function () {
    BookStore.removeChangeListener(this._onChange);
    PublisherStore.removeChangeListener(this._onChange);
    AuthorStore.removeChangeListener(this._onChange);
    NotificationStore.removeChangeListener(this._onChange);
  },

  _clearNotifications: function () {
    var notifications = this.state.notifications;
    var isStatus = notifications.status !== StatusConstants.IDLE;
    var isErrors = Object.keys(notifications.errors || {}).length;

    if (isStatus || isErrors) {
      NotificationActions.clearNotifications();
    }
  },

  _onChange: function () {
    this.setState({
      books: BookStore.getAllBooks(),
      publishers: PublisherStore.getAllPublishers(),
      authors: AuthorStore.getAllAuthors(),
      notifications: NotificationStore.getNotifications()
    });
  },

  _activate: function (e) {
    var target = e.target || e;

    if (target.nodeName === 'A') {
      target = target.parentNode;
    }

    document.querySelector('.active').className = '';
    target.className = 'active';
  },

  _renderChildren: function () {
    return React.Children.map(this.props.children, (function (child) {
      return React.cloneElement(child, {
        books: this.state.books,
        publishers: this.state.publishers,
        authors: this.state.authors,
        notifications: this.state.notifications
      });
    }).bind(this));
  },

  render: function () {
    return (
      <div>
        <header className="well container">
          <h4>Example 7 - Unidirectional Non-Functional Associations (Many-To-Many)</h4>
          <a href="https://bitbucket.org/workslon/master-thesis/wiki/chapter7.pdf">Related Thesis Chapter</a>
          &nbsp;/&nbsp;
          <a href="https://bitbucket.org/workslon/master-example-5/src">Source Code</a>
        </header>
        <div className="page-header">
          <h1><IndexLink to="/">Public Library</IndexLink></h1>
          <ul className="nav nav-tabs">
            <li ref="books" className="active" onClick={this._activate}><IndexLink to="/">Books</IndexLink></li>
            <li ref="publishers"><IndexLink onClick={this._activate} to="/publishers/list">Publishers</IndexLink></li>
            <li ref="authors"><IndexLink onClick={this._activate} to="/authors/list">Authors</IndexLink></li>
          </ul>
        </div>
        <div>
          {this._renderChildren()}
        </div>
      </div>
    );
  }
});