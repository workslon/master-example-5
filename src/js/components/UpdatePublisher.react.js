var React = require('react');
var ReactDOM = require('react-dom');
var PublisherActions = require('../actions/PublisherActions');
var PublisherModel = require('../models/Publisher');
var PublisherStore = require('../stores/PublisherStore');
var StatusConstants = require('../constants/StatusConstants');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  displayName: 'UpdatePublisher',

  componentWillMount: function() {
    this.publisher = PublisherStore.getPublisher(this.props.params.id);
  },

  componentDidMount: function () {
    ReactDOM.findDOMNode(this.refs.name).focus();
  },

  _updatePublisher: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var name = refs.name || {};

    PublisherActions.updatePublisher(PublisherModel, this.publisher, {
      name: name.value
    });
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    return (
        <div>
          <h3>Update Publisher</h3>
          {this.publisher ?
            <form>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <span>{this.publisher.name}</span>
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input defaultValue={this.publisher.email} disabled="disabled" ref="email" type="email" className="form-control" id="email" placeholder="Email" />
                {errors.email && <span className="text-danger">{errors.email}</span>}
              </div>
              <button type="submit" onClick={this._updatePublisher} className="btn btn-default">Submit</button>
              {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
              {status === StatusConstants.PENDING && <p className="bg-info">Updating...</p>}
              <IndexLink className="back" to="/publishers/list">&laquo; back</IndexLink>
            </form>
          : <div>No publisher found...</div>}
        </div>
    );
  }
});