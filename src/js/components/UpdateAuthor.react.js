var React = require('react');
var ReactDOM = require('react-dom');
var AuthorActions = require('../actions/AuthorActions');
var AuthorModel = require('../models/Author');
var AuthorStore = require('../stores/AuthorStore');
var StatusConstants = require('../constants/StatusConstants');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  displayName: 'UpdateAuthor',

  componentDidMount: function () {
    ReactDOM.findDOMNode(this.refs.name).focus();
  },

  componentWillMount: function() {
    this.author = AuthorStore.getAuthor(this.props.params.id);
  },

  _updateAuthor: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var name = refs.name || {};

    AuthorActions.updateAuthor(AuthorModel, this.author, {
      name: name.value
    });
  },

  _validate: function(e) {
    AuthorActions.validate(AuthorModel, e.target.id, e.target.value);
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    return (
        <div>
          <h3>Update Author</h3>
          {this.author ?
            <form>
              <div className="form-group">
                <label htmlFor="id">ID</label>
                <span>{this.author.authorId}</span>
              </div>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input defaultValue={this.author.name} onInput={this._validate} ref="name" type="name" className="form-control" id="name" placeholder="Name" />
                {errors.name && <span className="text-danger">{errors.name}</span>}
              </div>
              <button type="submit" onClick={this._updateAuthor} className="btn btn-default">Submit</button>
              {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
              {status === StatusConstants.PENDING && <p className="bg-info">Updating...</p>}
              <IndexLink className="back" to="/authors/list">&laquo; back</IndexLink>
            </form>
          : <div>No author found...</div>}
        </div>
    );
  }
});