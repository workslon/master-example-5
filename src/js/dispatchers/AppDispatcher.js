var flux = require('flux');
var dispatcher = new flux.Dispatcher();

module.exports = {
  register: function (callback) {
    return dispatcher.register(callback);
  },

  dispatch: function (action) {
    action || (action = {});
    console.log(action);
    dispatcher.dispatch(action);
  },

  dispatchAsync: function (promise, types, data) {
    var request = types.request;
    var success = types.success;
    var failure = types.failure;
    var dispatch = this.dispatch;

    data || (data = {});

    dispatch({
      type: request,
      data: data
    });

    promise.then(
      function (result) {
        dispatch({
          type: success,
          data: data,
          result: result
        });
      },

      function (error) {
        try {
          error = JSON.parse(error.responseText).error;
        } catch(e) {
          error = error.statusText;
        }

        dispatch({
          type: failure,
          data: data,
          error: error
        });
      }
    );
  }
};