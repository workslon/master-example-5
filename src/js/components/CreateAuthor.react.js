var React = require('react');
var ReactDOM = require('react-dom');
var AuthorActions = require('../actions/AuthorActions');
var AuthorModel = require('../models/Author');
var StatusConstants = require('../constants/StatusConstants');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  displayName: 'CreateAuthor',

  componentDidMount: function () {
    ReactDOM.findDOMNode(this.refs.authorId).focus();
  },

  componentDidUpdate: function () {
    var notifications = this.props.notifications || {};
    var status = notifications.status;

    if (status === StatusConstants.SUCCESS) {
      var refs = this.refs;
      refs.id.value = refs.name.value = '';
    }
  },

  _createAuthor: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var id = refs.id || {};
    var name = refs.name || {};

    AuthorActions.createAuthor(AuthorModel, {
      authorId: id.value,
      name: name.value
    });
  },

  _validate: function(e) {
    AuthorActions.validate(AuthorModel, e.target.id, e.target.value);
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    return (
      <form>
        <h3>Create Author</h3>
        <div className="form-group">
          <label htmlFor="id">ID</label>
          <input defaultValue="" ref="id" onInput={this._validate} type="text" className="form-control" id="authorId" placeholder="ID" />
          {errors.authorId && <span className="text-danger">{errors.authorId}</span>}
        </div>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input defaultValue="" ref="name" onInput={this._validate} type="name" className="form-control" id="name" placeholder="Name" />
          {errors.name && <span className="text-danger">{errors.name}</span>}
        </div>
        <button type="submit" onClick={this._createAuthor} className="btn btn-default">Submit</button>
        {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
        {status === StatusConstants.PENDING && <p className="bg-info">Creating...</p>}
        <IndexLink className="back" to="/authors/list">&laquo; back</IndexLink>
      </form>
    );
  }
});