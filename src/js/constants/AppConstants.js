var eNUMERATION = require('eNUMERATION');

module.exports = new eNUMERATION('BookConstants', [
  // Book
  'REQUEST_BOOKS',
  'REQUEST_BOOKS_SUCCESS',
  'REQUEST_BOOKS_ERROR',
  'REQUEST_BOOK_SAVE',
  'BOOK_SAVE_SUCCESS',
  'BOOK_SAVE_ERROR',
  'REQUEST_BOOK_UPDATE',
  'BOOK_UPDATE_SUCCESS',
  'BOOK_UPDATE_ERROR',
  'REQUEST_BOOK_DESTROY',
  'BOOK_DESTROY_SUCCESS',
  'BOOK_DESTROY_ERROR',
  'BOOK_VALIDATION_ERROR',
  'NON_UNIQUE_ISBN',

  // Publisher
  'REQUEST_PUBLISHERS',
  'REQUEST_PUBLISHERS_SUCCESS',
  'REQUEST_PUBLISHERS_ERROR',
  'REQUEST_PUBLISHER_SAVE',
  'PUBLISHER_SAVE_SUCCESS',
  'PUBLISHER_SAVE_ERROR',
  'REQUEST_PUBLISHER_UPDATE',
  'PUBLISHER_UPDATE_SUCCESS',
  'PUBLISHER_UPDATE_ERROR',
  'REQUEST_PUBLISHER_DESTROY',
  'PUBLISHER_DESTROY_SUCCESS',
  'PUBLISHER_DESTROY_ERROR',
  'PUBLISHER_VALIDATION_ERROR',
  'NON_UNIQUE_EMAIL',

  // Author
  'REQUEST_AUTHORS',
  'REQUEST_AUTHORS_SUCCESS',
  'REQUEST_AUTHORS_ERROR',
  'REQUEST_AUTHOR_SAVE',
  'AUTHOR_SAVE_SUCCESS',
  'AUTHOR_SAVE_ERROR',
  'REQUEST_AUTHOR_UPDATE',
  'AUTHOR_UPDATE_SUCCESS',
  'AUTHOR_UPDATE_ERROR',
  'REQUEST_AUTHOR_DESTROY',
  'AUTHOR_DESTROY_SUCCESS',
  'AUTHOR_DESTROY_ERROR',
  'AUTHOR_VALIDATION_ERROR',
  'NON_UNIQUE_ID',

  // Notifications
  'CLEAR_NOTIFICATIONS'
]);